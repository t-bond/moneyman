cmake_minimum_required(VERSION 3.15)
project(MoneyMan)

set(CMAKE_CXX_STANDARD 17)

# Get Qt dependencies
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Sql REQUIRED)

# Qt generator settings
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOUIC_SEARCH_PATHS ${CMAKE_SOURCE_DIR}/res/ui)
set(CMAKE_AUTORCC ON)

# Set project structure
include_directories(${CMAKE_SOURCE_DIR}/includes)

file(GLOB_RECURSE HEADERS includes/*.hpp)
file(GLOB_RECURSE SOURCES src/*.cpp)
file(GLOB_RECURSE RESOURCES res/*.qrc)

# Assemble the executable
add_executable(MoneyMan ${HEADERS} ${SOURCES} ${RESOURCES} src/model/DoubleSpinBox.cpp includes/model/DoubleSpinBox.hpp includes/model/TAX.hpp includes/model/Product.hpp includes/model/Unit.hpp src/model/ProductManager.cpp includes/model/ProductManager.hpp src/model/TAXManager.cpp includes/model/TAXManager.hpp src/model/UnitManager.cpp includes/model/UnitManager.hpp)
target_link_libraries(MoneyMan Qt5::Widgets)
target_link_libraries(MoneyMan Qt5::Sql)