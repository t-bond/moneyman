//
// Created by tbond on 09/09/2019.
//

#ifndef MONEYMAN_TAXMANAGER_HPP
#define MONEYMAN_TAXMANAGER_HPP


#include <memory>
#include <QtCore/QVector>
#include "TAX.hpp"

class TAXManager {
public:
    typedef std::shared_ptr<const TAX *> tax_ptr;

    TAXManager() = delete;
    [[nodiscard]] static tax_ptr getTAX(int group);

private:
    static inline QVector<tax_ptr> TAXGroups;
};

Q_DECLARE_METATYPE( TAXManager::tax_ptr)


#endif //MONEYMAN_TAXMANAGER_HPP
