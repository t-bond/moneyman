//
// Created by tbond on 06/09/2019.
//

#ifndef MONEYMAN_USER_HPP
#define MONEYMAN_USER_HPP

#include <QMetaType>

struct User {
    int id;
    QString name;
    int permission;
};

Q_DECLARE_METATYPE(User)

#endif //MONEYMAN_USER_HPP
