//
// Created by tbond on 09/09/2019.
//

#ifndef MONEYMAN_UNIT_HPP
#define MONEYMAN_UNIT_HPP

#include <QtCore/QString>
#include <utility>

struct Unit {
    Unit(const int id, QString name, const bool isWhole) : name(std::move(name)), isWhole(isWhole), id(id) {}
    const int id;
    const QString name;
    const bool isWhole;
};

#endif //MONEYMAN_UNIT_HPP
