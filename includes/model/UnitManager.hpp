//
// Created by tbond on 10/09/2019.
//

#ifndef MONEYMAN_UNITMANAGER_HPP
#define MONEYMAN_UNITMANAGER_HPP


#include <memory>
#include <QtCore/QMetaType>
#include "Unit.hpp"

class UnitManager {
public:
    typedef std::shared_ptr<const Unit *> unit_ptr;

    UnitManager() = delete;
    [[nodiscard]] static unit_ptr getUnit(int id);

private:
    static inline QVector<unit_ptr> units;
};

Q_DECLARE_METATYPE( UnitManager::unit_ptr)


#endif //MONEYMAN_UNITMANAGER_HPP
