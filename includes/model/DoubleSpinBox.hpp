//
// Created by tbond on 07/09/2019.
//

#ifndef MONEYMAN_DOUBLESPINBOX_HPP
#define MONEYMAN_DOUBLESPINBOX_HPP


#include <QtWidgets/QDoubleSpinBox>

class DoubleSpinBox : public QDoubleSpinBox {
    Q_OBJECT

public:
    explicit DoubleSpinBox(QWidget *parent = nullptr);

    signals:
        void returnPressed();

protected:
    void focusInEvent(QFocusEvent *event) override;

protected:
    void keyPressEvent(QKeyEvent *event) override;
};


#endif //MONEYMAN_DOUBLESPINBOX_HPP
