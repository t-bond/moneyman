//
// Created by tbond on 08/09/2019.
//

#ifndef MONEYMAN_PRODUCT_HPP
#define MONEYMAN_PRODUCT_HPP

#include <QtCore/QString>
#include <utility>
#include "TAX.hpp"
#include "Unit.hpp"
#include "TAXManager.hpp"
#include "UnitManager.hpp"

struct Product {
    Product(const int id, QString name, QString barcode, const double price, TAXManager::tax_ptr TAXGroup, UnitManager::unit_ptr unit) : id(id), name(std::move(name)),
                                                                                             barcode(std::move(barcode)),
                                                                                             price(price), tax_group(std::move(TAXGroup)), unit(std::move(unit)) {}

    const int id;
    const QString name, barcode;
    const double price;
    TAXManager::tax_ptr tax_group;
    UnitManager::unit_ptr unit;
};

#endif //MONEYMAN_PRODUCT_HPP
