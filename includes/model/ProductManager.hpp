//
// Created by tbond on 09/09/2019.
//

#ifndef MONEYMAN_PRODUCTMANAGER_HPP
#define MONEYMAN_PRODUCTMANAGER_HPP


#include <QtCore/QVector>
#include <QMetaType>
#include <memory>
#include "Product.hpp"

class ProductManager {
public:
    typedef std::shared_ptr<const Product *> product_ptr;

    ProductManager() = delete;
    [[nodiscard]] static product_ptr getProduct(int id);

private:
    static inline QVector<product_ptr> products;
};

Q_DECLARE_METATYPE( ProductManager::product_ptr)

#endif //MONEYMAN_PRODUCTMANAGER_HPP
