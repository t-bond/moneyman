//
// Created by tbond on 08/09/2019.
//

#ifndef MONEYMAN_TAX_HPP
#define MONEYMAN_TAX_HPP

#include <QtCore/QString>
#include <utility>
#include <QMetaType>

struct TAX {
    TAX(const int id, const double value, QString name, QString alternativeName) : id(id), value(value),
                                                                                                 name(std::move(name)),
                                                                                                 alternative_name(std::move(
                                                                                                         alternativeName)) {}

    const int id;
    const double value;
    const QString name, alternative_name;
};

#endif //MONEYMAN_TAX_HPP
