//
// Created by tbond on 05/09/2019.
//

#ifndef MONEYMAN_UTILITIES_HPP
#define MONEYMAN_UTILITIES_HPP


#include <QtWidgets/QMainWindow>
#include <QtSql/QSqlQuery>

class Utilities {

public:
    Utilities() = delete;
    static void updateDatabase();
    static void updateWindowSizeRestrictions(QMainWindow *window);
    static void centerWindow(QMainWindow *window);
    static int getRowCount(QSqlQuery &query);
};


#endif //MONEYMAN_UTILITIES_HPP
