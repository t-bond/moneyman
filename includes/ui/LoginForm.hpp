//
// Created by tbond on 05/09/2019.
//

#ifndef MONEYMAN_LOGINFORM_HPP
#define MONEYMAN_LOGINFORM_HPP

#include "ui_LoginForm.h"
#include "model/User.hpp"

class LoginForm : public QWidget {
Q_OBJECT

public:
    explicit LoginForm(QWidget *parent = nullptr);

signals:

    void loginSuccessful(User userData);

private:
    Ui::LoginForm ui;
    int userCount = 0;

private slots:

    void doLogin();
};


#endif //MONEYMAN_LOGINFORM_HPP
