//
// Created by tbond on 07/09/2019.
//

#ifndef MONEYMAN_ABOUT_DIALOG_HPP
#define MONEYMAN_ABOUT_DIALOG_HPP

#include "ui_AboutDialog.h"

class AboutDialog : public QDialog {
Q_OBJECT

public:
    explicit AboutDialog(QDialog *parent = nullptr);

private:
    Ui::AboutDialog ui;
};


#endif //MONEYMAN_ABOUT_DIALOG_HPP
