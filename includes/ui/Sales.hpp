//
// Created by tbond on 06/09/2019.
//

#ifndef MONEYMAN_SALES_HPP
#define MONEYMAN_SALES_HPP

#include <model/Product.hpp>
#include <model/ProductManager.hpp>
#include "ui_Sales.h"
#include "model/User.hpp"

class Sales : public QWidget {
Q_OBJECT

public:
    explicit Sales(QWidget *parent = nullptr);

    void setUserData(User loggedInUserData);

signals:
    void onExit();

private:
    User userData;
    Ui::Sales ui;
    ProductManager::product_ptr currentItem = nullptr;
    int currentlyEditedRow = -1;

    const QString currency = "HUF";
    double totalValue = 0;

    double getCurrentValue();

private slots:
    void onUniversalInput(const QString& text);
    void onUniversalInput();
    void onProductSelect(int, int);
    void onProductEdit(int, int);
    void addProduct();
    void updateCheckedProductRow(int row, const ProductManager::product_ptr& product);
    void countChange();
    void stornoItem();
    void clearProductInfoPanel(bool keepSearch = false, bool keepCount = false);
    void showProductInfo(ProductManager::product_ptr& product);
};


#endif //MONEYMAN_SALES_HPP
