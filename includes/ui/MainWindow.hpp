//
// Created by tbond on 06/09/2019.
//

#ifndef MONEYMAN_MAIN_WINDOW_HPP
#define MONEYMAN_MAIN_WINDOW_HPP

#include "ui_MainWindow.h"
#include "LoginForm.hpp"
#include "Sales.hpp"

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    Ui::MainWindow ui;
    LoginForm *login = nullptr;
    Sales *sales = nullptr;

private slots:

    void onLoginSuccess(const User& userData);

    void onLogout();
};


#endif //MONEYMAN_MAIN_WINDOW_HPP
