//
// Created by tbond on 05/09/2019.
//

#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QSqlDriver>
#include <QSqlError>
#include "Utilities.hpp"
#include <QApplication>
#include <QScreen>

void Utilities::updateDatabase() {
    QFile file(":Database.sql");
    if (!file.open(QIODevice::ReadOnly)) {
        qInfo("Couldn't open database updater file.");
        return;
    }

    // The SQLite driver executes only a single (the first) query in the QSqlQuery
    //  if the script contains more queries, it needs to be splitted.
    QStringList scriptQueries = QTextStream(&file).readAll().split(';');
    QSqlQuery query(QSqlDatabase::database());
            foreach(QString queryTxt, scriptQueries) {
            if (queryTxt.trimmed().isEmpty())
                continue;

            if (!query.exec(queryTxt))
                qInfo("One of the query failed to execute. Error detail: %s",
                      query.lastError().text().toStdString().c_str());
            query.finish();
        }

    file.close();
}

void Utilities::updateWindowSizeRestrictions(QMainWindow *window) {
    auto *centralWidget = window->centralWidget();
    window->setMaximumSize(centralWidget->maximumSize());
    window->setMinimumSize(centralWidget->minimumSize());
}

void Utilities::centerWindow(QMainWindow *window) {
    window->move(QApplication::primaryScreen()->geometry().center() - window->geometry().center());
}

int Utilities::getRowCount(QSqlQuery &query) {
    if (query.driver()->hasFeature(QSqlDriver::QuerySize))
        return query.size();

    query.last();
    return query.at() + 1;
}
