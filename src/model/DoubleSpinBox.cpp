//
// Created by tbond on 07/09/2019.
//

#include <QKeyEvent>
#include <QTimer>
#include "model/DoubleSpinBox.hpp"

DoubleSpinBox::DoubleSpinBox(QWidget *parent) : QDoubleSpinBox(parent) {
}

void DoubleSpinBox::keyPressEvent(QKeyEvent *event) {
    QAbstractSpinBox::keyPressEvent(event);

    if(event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
        emit returnPressed();
}

void DoubleSpinBox::focusInEvent(QFocusEvent *event) {
    QAbstractSpinBox::focusInEvent(event);

    QTimer::singleShot(0, this, &QDoubleSpinBox::selectAll);
}
