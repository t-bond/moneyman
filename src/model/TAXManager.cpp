//
// Created by tbond on 09/09/2019.
//

#include <QtSql/QSqlQuery>
#include <QVariant>
#include <Utilities.hpp>
#include "model/TAXManager.hpp"

TAXManager::tax_ptr TAXManager::getTAX(int group) {
    for(auto& tax : TAXGroups)
        if((*tax)->id == group)
            return tax;

    QSqlQuery query;
    query.prepare("SELECT `name`, `alternative_name`, `value` FROM `tax_groups` WHERE `id` = ? LIMIT 1");
    query.addBindValue(group);
    query.exec();

    if(Utilities::getRowCount(query) < 1)
        return nullptr;

    tax_ptr tax = std::make_unique<const TAX *>(new TAX(group, query.value("value").toDouble(), query.value("name").toString(), query.value("alternative_name").toString()));
    TAXGroups.append(tax);
    return tax;
}
