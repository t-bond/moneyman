//
// Created by tbond on 10/09/2019.
//

#include <QtSql/QSqlQuery>
#include <QVariant>
#include <Utilities.hpp>
#include "model/UnitManager.hpp"

UnitManager::unit_ptr UnitManager::getUnit(int id) {
    for(auto& unit : units)
        if((*unit)->id == id)
            return unit;

    QSqlQuery query;
    query.prepare("SELECT `name`, `is_whole` FROM `units` WHERE `id` = ? LIMIT 1");
    query.addBindValue(id);
    query.exec();

    if(Utilities::getRowCount(query) < 1)
        return nullptr;

    unit_ptr unit = std::make_unique<const Unit *>(new Unit(id, query.value("name").toString(), query.value("is_whole").toBool()));
    units.append(unit);
    return unit;
}
