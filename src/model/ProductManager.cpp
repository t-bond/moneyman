//
// Created by tbond on 09/09/2019.
//

#include <model/TAXManager.hpp>
#include <QtSql/QSqlQuery>
#include <QVariant>
#include <Utilities.hpp>
#include <model/UnitManager.hpp>
#include "model/ProductManager.hpp"

ProductManager::product_ptr ProductManager::getProduct(int id) {
    for(auto& product : products)
        if((*product)->id == id)
            return product;

    QSqlQuery query;
    query.prepare("SELECT * FROM `products` WHERE `id` = ? LIMIT 1");
    query.addBindValue(id);
    query.exec();

    if(Utilities::getRowCount(query) < 1)
        return nullptr;

    product_ptr product = std::make_unique<const Product *>(new Product(id, query.value("name").toString(), query.value("barcode").toString(), query.value("selling_price").toDouble(), TAXManager::getTAX(query.value("tax_group").toInt()), UnitManager::getUnit(query.value("unit").toInt())));
    products.append(product);
    return product;
}