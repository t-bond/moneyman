//
// Created by tbond on 06/09/2019.
//

#include <Utilities.hpp>
#include "ui/AboutDialog.hpp"
#include "ui/MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    ui.setupUi(this);
    onLogout();

    connect(ui.action_Credits, &QAction::triggered, this, [=]() {
        AboutDialog ad;
        ad.exec();
    });
}

void MainWindow::onLoginSuccess(const User& userData) {
    if (login != nullptr) {
        login->deleteLater();
        login = nullptr;
    }
    if (sales == nullptr)
        sales = new Sales();
    setCentralWidget(sales);
    sales->setUserData(userData);
    setWindowTitle(sales->windowTitle());
    connect(sales, &Sales::onExit, this, &::MainWindow::onLogout);

    ui.action_Logout->setVisible(true);
    Utilities::updateWindowSizeRestrictions(this);
    Utilities::centerWindow(this);
}

void MainWindow::onLogout() {
    if (login == nullptr)
        login = new LoginForm();
    if (sales != nullptr) {
        sales->deleteLater();
        sales = nullptr;
    }

    setCentralWidget(login);
    ui.statusbar->clearMessage();
    setWindowTitle(login->windowTitle());
    connect(login, &LoginForm::loginSuccessful, this, &MainWindow::onLoginSuccess);

    ui.action_Logout->setVisible(false);
    Utilities::updateWindowSizeRestrictions(this);
    Utilities::centerWindow(this);
}
