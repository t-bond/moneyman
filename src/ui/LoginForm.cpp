//
// Created by tbond on 05/09/2019.
//

#include "ui/LoginForm.hpp"
#include <QSqlDriver>
#include <QtSql/QSqlQuery>
#include <QScreen>
#include <QCryptographicHash>
#include <QtWidgets/QStatusBar>
#include <Utilities.hpp>

LoginForm::LoginForm(QWidget *parent) : QWidget(parent) {
    ui.setupUi(this);

    QSqlQuery query;
    query.exec("SELECT `id`, `user`, `permission` FROM `users` WHERE `enabled` = TRUE;");

    bool showAdmins = QApplication::arguments().contains("--show-admins");
    while (query.next()) {
        int id = query.value("id").toInt(),
                permission = query.value("permission").toInt();
        if (permission == 0 && !showAdmins)
            continue;

        ui.userSelector->addItem(query.value("user").toString(), id);
        userCount++;
    }

    if (userCount < 2)
        ui.userSelector->setEnabled(false);
}

void LoginForm::doLogin() {
    ui.userSelector->setEnabled(false);
    ui.password->setEnabled(false);

    auto statusbar = QApplication::activeWindow()->findChild<QStatusBar *>("statusbar");
    if (ui.password->text().isEmpty())
        statusbar->showMessage(tr("Password can not be empty!"));
    else {
        QSqlQuery query;
        int userID = ui.userSelector->currentData().toInt();
        query.prepare("SELECT `user`, `enabled`, `permission` FROM `users` WHERE `id` = ? AND `password` = ? LIMIT 1;");
        query.addBindValue(userID);
        query.addBindValue(QString(QCryptographicHash::hash(ui.password->text().toUtf8(),
                                                            QCryptographicHash::Sha1).toHex()));
        query.exec();

        if (Utilities::getRowCount(query) == 1 && query.value("enabled").toBool()) {
            // Login successful
            ui.password->clear();
            statusbar->clearMessage();

            emit loginSuccessful({userID, query.value("user").toString(), query.value("permission").toInt()});
        } else
            statusbar->showMessage(tr("Wrong password!"));
    }

    ui.userSelector->setEnabled(userCount > 1);
    ui.password->setEnabled(true);
}