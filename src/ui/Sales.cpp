//
// Created by tbond on 06/09/2019.
//

#include "ui/Sales.hpp"

#include <utility>
#include <QtWidgets/QStatusBar>
#include <QtSql/QSqlQuery>
#include <QSqlDriver>
#include <QtWidgets/QPushButton>
#include <Utilities.hpp>
#include <QTimer>
#include <model/ProductManager.hpp>

Sales::Sales(QWidget *parent) : QWidget(parent) {
    ui.setupUi(this);

    ui.checkedProducts->resizeColumnToContents(0); // Resize the delete buttons' column to the minimum width

    // Insert the new product spanned row at the bottom.
    ui.checkedProducts->insertRow(0);
    ui.checkedProducts->setSpan(0, 0, 1, ui.checkedProducts->columnCount());
    auto *newProductButton = new QPushButton(this);
    newProductButton->setText(tr("Check new product"));
    ui.checkedProducts->setCellWidget(0, 0, newProductButton);
    connect(newProductButton, &QPushButton::clicked, this, [=](){
        clearProductInfoPanel();
        ui.searchInput->setFocus();
    });

    QTimer::singleShot(0, this, [=](){ ui.searchInput->setFocus(); }); // Set the focus to the universal input

    // Set up currencies
    ui.currencyLabel->setText(currency);
    ui.currencyLabel_2->setText(currency);
    ui.total->setText(QString::number(totalValue) + " " + currency);
}

void Sales::setUserData(User loggedInUserData) {
    this->userData = std::move(loggedInUserData);

    auto statusbar = QApplication::activeWindow()->findChild<QStatusBar *>("statusbar");
    statusbar->showMessage(tr("Logged in as: %1").arg(this->userData.name));
}

void Sales::onUniversalInput(const QString& text) {
    bool isShortcode = text == "/";
    if(text.isEmpty() || isShortcode) { // If the input is empty, or started shortcode, don't do anything only show the info panel
        ui.currentProductPanel->setCurrentIndex(0);
        return;
    }

    // Check for the `number*` pattern to fast count setting
    QRegExp regexp("([0-9]*(?:\\.[0-9]*)?)\\*");
    if(regexp.indexIn(text) != -1) { // If a pattern was recognised
        ui.countInput->setValue(regexp.cap(1).toDouble()); // Update the value
        ui.searchInput->clear();
        ui.currentProductPanel->setCurrentIndex(0);
        return; // Don't need to continue checking
    }


    QString shortcode = "";
    QSqlQuery query;
    regexp.setPattern("\\/([0-9]*)");
    if(regexp.indexIn(text) != -1) { // If the user typed in a shortcode
        isShortcode = true;
        shortcode = regexp.cap(1);
        query.prepare("SELECT `id` FROM `products` WHERE `barcode` = ?");
        query.addBindValue(shortcode); // Try to find it
        query.exec();
    }else{ // Else try to do an universal search
        QString searchTerm = QString("%%%1%").arg(text);
        query.prepare("SELECT `id` FROM `products` WHERE `name` LIKE :text OR `barcode` LIKE :text");
        query.bindValue(":text", searchTerm);
        query.exec();
    }

    int numRows = Utilities::getRowCount(query);
    if(numRows <= 0) { // Show error when nothing matches
        ui.currentProductPanel->setCurrentIndex(0);

        auto statusbar = QApplication::activeWindow()->findChild<QStatusBar *>("statusbar");
        if(isShortcode) // Show different message on missing shortcode
            statusbar->showMessage(tr("No product with the ,,%1\" shortcode was found.").arg(shortcode), 2000);
        else
            statusbar->showMessage(tr("No result for the given term: ,,%1\"").arg(text), 2000);
        clearProductInfoPanel(true, true); // Clear the current info from the panel
        return;
    }

    if(numRows == 1) { // If only one match is found
        currentItem = ProductManager::getProduct(query.value("id").toInt());
        showProductInfo(currentItem);
    }else{
        ui.productResults->setRowCount(numRows);
        int row = 0;
        query.first();
        do { // Add all the matching data to the table
            ProductManager::product_ptr product = ProductManager::getProduct(query.value("id").toInt());

            auto *productBarcode = new QTableWidgetItem((*product)->barcode);
            productBarcode->setData(Qt::UserRole, QVariant::fromValue(product)); // Store the product pointer on the barcode cell
            ui.productResults->setItem(row, 0, productBarcode);
            ui.productResults->setItem(row, 1, new QTableWidgetItem((*product)->name));
            ui.productResults->setItem(row, 2, new QTableWidgetItem(QString::number((*product)->price) + " " + currency));
            ui.productResults->setItem(row, 3, new QTableWidgetItem((*(*product)->tax_group)->name));
            row++;
        } while(query.next());
        ui.productResults->update();
        ui.currentProductPanel->setCurrentIndex(1); // Show the product list table
    }
}

void Sales::onProductSelect(int row, int) {
    currentItem = ui.productResults->item(row, 0)->data(Qt::UserRole).value<ProductManager::product_ptr>();
    showProductInfo(currentItem);
}

void Sales::addProduct() {
    if(!currentItem)
        return;

    int row;
    if(currentlyEditedRow == -1) {
        row = ui.checkedProducts->rowCount() - 1;
        ui.checkedProducts->insertRow(row);
        auto *removeButton = new QToolButton();
        removeButton->setText("✖");
        connect(removeButton, &QToolButton::clicked, this, &Sales::stornoItem);
        ui.checkedProducts->setCellWidget(row, 0, (QWidget *) removeButton);
    }else {
        row = currentlyEditedRow;
        totalValue -= ui.checkedProducts->item(row, 5)->data(Qt::UserRole).toDouble() * (*currentItem)->price;
    }

    updateCheckedProductRow(row, currentItem);

    totalValue += getCurrentValue();
    ui.total->setText(QString::number(totalValue) + " " + currency);


    ui.searchInput->setFocus();
    clearProductInfoPanel();
}

void Sales::countChange() {
    ui.productValue->setText(QString::number(getCurrentValue()));
}

void Sales::stornoItem() {
    for (int row = ui.checkedProducts->rowCount() - 1; row >= 0; --row)
        if(ui.checkedProducts->cellWidget(row, 0) == QObject::sender()) {
            totalValue -= ui.checkedProducts->item(row, 4)->data(Qt::UserRole).toDouble() *
                        (*ui.checkedProducts->item(row, 1)->data(Qt::UserRole).value<ProductManager::product_ptr>())->price;
            ui.total->setText(QString::number(totalValue) + " " + currency);
            ui.checkedProducts->removeRow(row);
            return;
        }
}

void Sales::onProductEdit(int row, int) {
    if(row == ui.checkedProducts->rowCount())
        return;

    currentItem = ui.checkedProducts->item(row, 1)->data(Qt::UserRole).value<ProductManager::product_ptr>();
    currentlyEditedRow = row;
    ui.countInput->setValue(ui.checkedProducts->item(row, 4)->data(Qt::UserRole).toDouble());
    showProductInfo(currentItem);
    ui.countInput->setFocus();
}

void Sales::clearProductInfoPanel(bool keepSearch, bool keepCount) {
    ui.productBarcode->clear();
    ui.productName->clear();
    ui.productPrice->setText("0");
    ui.productValue->setText("0");
    ui.countInput->setSuffix("");

    if(!keepSearch)
        ui.searchInput->clear();
    if(!keepCount) {
        ui.countInput->setValue(1);
        ui.countInput->setDecimals(2);
    }
    currentlyEditedRow = -1;
    currentItem = nullptr;
    ui.currentProductPanel->setCurrentIndex(0);
}

void Sales::onUniversalInput() {
    onUniversalInput(ui.searchInput->text());
}

void Sales::showProductInfo(ProductManager::product_ptr& product) {
    if(!product)
        return;
    currentItem = product;

    ui.productName->setText((*currentItem)->name);
    ui.productPrice->setText(QString::number((*currentItem)->price));
    ui.productBarcode->setText((*currentItem)->barcode);
    countChange();

    const Unit *unit = *(*currentItem)->unit;
    ui.countInput->setSuffix(" " + unit->name);
    ui.countInput->setDecimals(unit->isWhole ? 0 : 2);

    ui.currentProductPanel->setCurrentIndex(0);
}

void Sales::updateCheckedProductRow(int row, const ProductManager::product_ptr& product) {
    auto *productBarcode = new QTableWidgetItem((*product)->barcode);
    productBarcode->setData(Qt::UserRole, QVariant::fromValue(currentItem));
    ui.checkedProducts->setItem(row, 1, productBarcode);
    ui.checkedProducts->setItem(row, 2, new QTableWidgetItem((*product)->name));
    ui.checkedProducts->setItem(row, 3, new QTableWidgetItem(QString::number((*product)->price) + " " + currency));
    auto *countWidget = new QTableWidgetItem(QString::number(ui.countInput->value()) + " " + (*(*product)->unit)->name);
    countWidget->setData(Qt::UserRole, ui.countInput->value());
    ui.checkedProducts->setItem(row, 4, countWidget);
    ui.checkedProducts->setItem(row, 5, new QTableWidgetItem(ui.productValue->text() + " " + currency));
}

double Sales::getCurrentValue() {
    if(currentItem)
        return (*currentItem)->price * ui.countInput->value();
    return 0;
}
