#include <QApplication>
#include <QtCore/QString>
#include <QSqlDatabase>
#include <QSqlError>
#include <QScreen>
#include <QtSql/QSqlQuery>
#include "Utilities.hpp"
#include "ui/MainWindow.hpp"

const char *DRIVER = "QSQLITE",
        *DATABASE = "data.bin";

int main(int argc, char *argv[]) {
    // Database connection
    if (!QSqlDatabase::isDriverAvailable(DRIVER))
        qFatal("Database driver isn't available! (%s)", DRIVER);

    QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);
    db.setDatabaseName(DATABASE);
    if (!db.open())
        qFatal("ERROR: %s", db.lastError().text().toStdString().c_str());

    QSqlQuery q("PRAGMA foreign_keys = ON;");
    q.exec();

    Utilities::updateDatabase();

    QApplication app(argc, argv);
    MainWindow mw;
    mw.show();
    int returnValue = QApplication::exec();
    db.close();
    return returnValue;
}