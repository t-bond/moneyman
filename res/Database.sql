CREATE TABLE IF NOT EXISTS 'users'
(
    'id'           INTEGER NOT NULL CONSTRAINT `users_pk` PRIMARY KEY AUTOINCREMENT,
    'user'         TEXT NOT NULL,
    'password'     TEXT NOT NULL,
    'permission'   INTEGER NOT NULL DEFAULT 2,
    'enabled'      BOOLEAN NOT NULL DEFAULT 1,
    'registration' DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX IF NOT EXISTS `users_id_uindex` ON `users` (`id`);

CREATE TABLE IF NOT EXISTS 'products'
(
    'id' INTEGER NOT NULL CONSTRAINT `products_pk` PRIMARY KEY AUTOINCREMENT,
    'name' TEXT NOT NULL,
    'purchase_price' REAL,
    'selling_price' REAL,
    'tax_group' INTEGER NOT NULL CONSTRAINT 'tax_group_fk' REFERENCES `tax_groups` ON UPDATE CASCADE ON DELETE RESTRICT,
    'barcode' TEXT,
    'unit' INTEGER constraint units_fk
        references units
        on update cascade on delete restrict
);

CREATE UNIQUE INDEX IF NOT EXISTS `products_id_uindex` ON `products` (`id`);

CREATE TABLE IF NOT EXISTS `tax_groups`
(
    'id' INTEGER NOT NULL CONSTRAINT `tax_groups_pk` PRIMARY KEY AUTOINCREMENT,
    'value' REAL NOT NULL,
    'name' TEXT NOT NULL,
    'alternative_name' TEXT
);

CREATE UNIQUE INDEX IF NOT EXISTS `tax_groups_id_uindex` ON `tax_groups` (`id`);

create table IF NOT EXISTS units
(
    id INTEGER not null
        constraint units_pk
            primary key autoincrement,
    name TEXT not null,
    is_whole BOOLEAN default 1 not null
);

create unique index IF NOT EXISTS units_id_uindex
    on units (id);

